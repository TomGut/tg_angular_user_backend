import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../services/users.service';
import {UserList} from '../../models/user-list';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private usersService: UsersService) { }

  users: UserList;

  ngOnInit() {
    return this.usersService.getUsersAsList().subscribe(user => (
      this.users = user
    ));
  }

}
